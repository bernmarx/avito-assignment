module github.com/bernmarx/avito-assignment

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/getsentry/sentry-go v0.12.0
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
	github.com/stretchr/testify v1.7.0
)
